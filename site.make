core = 7.x
api = 2

; uw_ct_publication
projects[uw_ct_publication][type] = "module"
projects[uw_ct_publication][download][type] = "git"
projects[uw_ct_publication][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_publication.git"
projects[uw_ct_publication][download][tag] = "7.x-1.4"
projects[uw_ct_publication][subdir] = ""
